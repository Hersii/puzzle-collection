# Puzzle Collection

A Puzzle Collection több különböző rejtvényt tartalmaz. 
Ezeknek célja, a gondolkodtató, játékos feladványokkal szórakoztatni a felhasználót. 
A főmenüben láthatjuk a játékok listáját, valamint onnan indíthatjuk el őket, vagy léphetünk vissza, amennyiben másik játékkal szeretnénk játszani.
Sok játéknál nehézség is választható, így a kezdőktől a profikig mindenki élvezheti, valamint néhol beépített bot ellen is játszhatunk, vagy egymás ellen.

Megvalósítás nyelve: Java

ELTE IK Projekt eszközök 2018-19-2